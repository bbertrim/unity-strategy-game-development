﻿//
//  Mobile.cs
//
//  Author:
//       Bradley Bertrim <bertrim.bradley@gmail.com>
//
//  Copyright (c) 2014 Bradley Bertrim
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using UnityEngine;
using System.Collections;
using Bradley.Game.Core.Logging;
namespace Bradley.Game.RTS {

    [RequireComponent (typeof(Rigidbody))]
    abstract public class Mobile:MonoBehaviour{
        public float speed = 50;
        public float deadRange = 1;
        protected Log logger = null;
        private Vector3? target = null;

        abstract public void InteractWith();

        public Mobile(){
            logger = new Log(this.GetType());
        }

        public void Move(Vector3 location){
            logger.debug(gameObject.name + " Moving to " + location);
            target = location;
        }

        public void FixedUpdate(){
            if(target != null){
                logger.trace("target != null");
                float distance = Vector3.Distance(transform.position, 
                                                  (Vector3)target);
                logger.trace("The distance to target is " + distance);
                if(distance > deadRange){
                    logger.trace("target not in range");
                    gameObject.transform.LookAt((Vector3)target);
                    Vector3 force = speed * transform.forward;
                    rigidbody.AddForce( force * Time.smoothDeltaTime );
                }
            }
        }
    }
}