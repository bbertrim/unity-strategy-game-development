//
//  UnitManager.cs
//
//  Author:
//       Bradley Bertrim <bertrim.bradley@gmail.com>
//
//  Copyright (c) 2014 Bradley Bertrim
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Bradley.Game.Core.Logging;
using Bradley.Unity.Type;

namespace Bradley.Game.RTS {
    public class UnitManager {
        
        public Action LeftMouseBinding = Action.Select;
        public Action RightMouseBinding = Action.Interact;
        public Action MiddleMouseBinding = Action.Select;
        private static UnitManager instance = null;
        private List<GameObject> selectedUnits = new List<GameObject>();
        private Log logger = null;

        private UnitManager() {          
            logger = new Log(this.GetType());
        }

        public static UnitManager Instance {
            get {
                //If no selection manager instance exists create it
                if(instance == null) {
                    instance = new UnitManager();
                }
                
                return instance;
            }
        }


        /// <summary>
        /// Public interface for mouse clicks. Messages are passed into here and 
        /// then the proper handler gets called based on the key mapping.
        /// </summary>
        /// <param name="mouseButton">Mouse button.</param>
        /// <param name="targetGameObject">Target game object.</param>
        /// <param name="location">Location.</param>
        /// <param name="modifiers">Modifiers.</param>
        public void ClickNotification(MouseButton mouseButton, 
                                      GameObject targetGameObject, 
                                      Vector3 location, 
                                      List<KeyCode> modifiers) {
            logger.debug("ClickNotification received for mouse button " 
                         + mouseButton.ToString());
            switch(mouseButton) {
            case MouseButton.Left:
                MapMouseButtonToActionHandler(LeftMouseBinding, 
                                              targetGameObject, 
                                              location, 
                                              modifiers);
                break;
            case MouseButton.Right:
                MapMouseButtonToActionHandler(RightMouseBinding, 
                                              targetGameObject, 
                                              location, 
                                              modifiers);
                break;
            case MouseButton.Middle:
                MapMouseButtonToActionHandler(MiddleMouseBinding, 
                                              targetGameObject, 
                                              location, 
                                              modifiers);
                break;
            }
        
        }

        /// <summary>
        /// Maps the Mouse Button to an Action Handler.
        /// </summary>
        /// <param name="action">Action.</param>
        /// <param name="targetGameObject">Target game object.</param>
        /// <param name="location">Location.</param>
        /// <param name="modifiers">Modifiers.</param>
        private void MapMouseButtonToActionHandler(Action action, 
                                                   GameObject targetGameObject, 
                                                   Vector3 location, 
                                                   List<KeyCode> modifiers) {
            switch(action) {
            case Action.Select:
                SelectActionHandler(targetGameObject, location, modifiers);
                break;
            case Action.Interact:                
                InteractActionHandler(targetGameObject, location, modifiers);
                break;                                        
            case Action.Manipulate:
                ManipulateActionHandler(targetGameObject, location, modifiers);
                break;
            }
        }


        /// <summary>
        /// Selection Action Handler is responsible a select action.
        /// </summary>
        /// <param name="targetGameObject"></param>
        /// <param name="location"></param>
        /// <param name="modifiers"></param>
        private void SelectActionHandler(GameObject targetGameObject, 
                                         Vector3 location, 
                                         List<KeyCode> modifiers) {
            logger.debug("Select on " + targetGameObject.name);        
            if(!modifiers.Contains(KeyCode.LeftShift) && 
               !modifiers.Contains(KeyCode.RightShift)) {          
                clearSelection();            
                selectUnit(targetGameObject);
            } else {
                toggleUnitSelection(targetGameObject);
            }         
        }
  
        // ///////////////////////  
        //Selection Flunky Methods

        private void selectUnit(GameObject unit) {
            logger.debug(unit.name + 
                         " selected, adding it to the selection list");
            if(unit.GetComponent("UnitMobile") != null) {
                if(!selectedUnits.Contains(unit)) {
                    selectedUnits.Add(unit);                 
                }
                logger.debug("Adding halo to to " + unit.name);
                (unit.GetComponent("Halo") as Behaviour).enabled = true;
            }
        }
    
        private void toggleUnitSelection(GameObject unit) {
            logger.debug("Toggling selection on " + unit.name); 
      
            if(selectedUnits.Contains(unit)) {
                deselectUnit(unit);
            } else {
                selectUnit(unit);
            }
        }
    
        private void deselectUnit(GameObject unit) {
            logger.debug("clear selection on unit " + unit.name);
            selectedUnits.Remove(unit);
            (unit.GetComponent("Halo") as Behaviour).enabled = false;
        }
    
        private void clearSelection() {
            logger.debug("cearling selection");
            foreach(GameObject unit in selectedUnits) {
                logger.debug("removing halo from " + unit.name);
                (unit.GetComponent("Halo") as Behaviour).enabled = false;
            }
            selectedUnits.Clear();
        }
    
      
        /// <summary>
        /// Interact Action Handler is responsible for an interact action such 
        /// as attack or move commands. 
        /// </summary>
        /// <param name="targetGameObject"></param>
        /// <param name="location"></param>
        /// <param name="modifiers"></param>
        private void InteractActionHandler(GameObject targetGameObject, 
                                           Vector3 location, 
                                           List<KeyCode> modifiers) {
            logger.debug("Interact on " + targetGameObject.name);

            //Tell selected units an interaction has occured
            InteractWithSelectedUnits(targetGameObject, location);

            //Tell targeted unit that it is being interacted with
            NotifyInteractionWithUnit(targetGameObject);
        }

        private void InteractWithSelectedUnits(GameObject targetGameObject, 
                                               Vector3 location) {
            foreach(GameObject selectedUnit in selectedUnits){
                UnitMobile unitMobile = 
                    (UnitMobile)selectedUnit.GetComponent(typeof(UnitMobile));

                unitMobile.Move(location);
            }
        }

        private void NotifyInteractionWithUnit(GameObject targetGameObject) {
            UnitMobile targetUnit = 
                (UnitMobile)targetGameObject.GetComponent(typeof(UnitMobile));
            //If we clicked on a UnitMobile
            if(targetUnit != null){
                targetUnit.InteractWith();
            } else {
                logger.debug("Interaction with " + targetGameObject.name + 
                             " detected. Since this object is not a mobile it" + 
                             " does cannot respond to the interaction");
            }
        }

        /// <summary>
        /// Manipulate Action Handler is responsible for a manipulate action 
        /// such as contextual commands.
        /// </summary>
        /// <param name="targetGameObject"></param>
        /// <param name="location"></param>
        /// <param name="modifiers"></param>
        private void ManipulateActionHandler(GameObject targetGameObject, 
                                             Vector3 location, 
                                             List<KeyCode> modifiers) {
            logger.debug("Manipulate on " + targetGameObject.name);
        }
    }
}