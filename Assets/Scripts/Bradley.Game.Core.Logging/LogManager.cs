//
//  LogManager.cs
//
//  Author:
//       Bradley Bertrim <bertrim.bradley@gmail.com>
//
//  Copyright (c) 2014 Bradley Bertrim
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using UnityEngine;
using System;
using System.Collections.Generic;

namespace Bradley.Game.Core.Logging {
    public class LogManager : MonoBehaviour {
        private static LogManager instance = null;
        public LogLevel DefaultLogLevel = LogLevel.INFO;

        public List<MappedLogLevel> mappedLogLevels = 
                                        new List<MappedLogLevel>();

        public LogManager() {            
            init(this);
        }

        public static void init(LogManager newInstance) {
            instance = newInstance;
        }

        public static LogManager Instance {
            get {
                return instance;
            }
        }

        /// <summary>
        /// Logs a message if the default/class log level is set low enough.
        /// </summary>
        /// <param name="className">String with the fully quallified class name.
        /// </param>
        /// <param name="message">String message to print.</param>
        /// <param name="logLevel">The log level of the message.</param>
        public void LogMessage(string className, 
                               string message, 
                               LogLevel logLevel) {
            if(message != null && message.Trim().Length > 0) {       
                MappedLogLevel mappedlogLevelToUse = 
                    LogLevelUtil.getMappedLogLevelByClass(className, 
                                                          mappedLogLevels);
                LogLevel logLevelToUse = DefaultLogLevel;
                
                if(mappedlogLevelToUse != null) {
                    logLevelToUse = mappedlogLevelToUse.LogLevel;
                }

                if((int)logLevel >= (int)logLevelToUse) {
                    Debug.Log(logLevel.ToString() + " " + 
                              className + " " + 
                              message);
                }          
            }
        }
    }
}
