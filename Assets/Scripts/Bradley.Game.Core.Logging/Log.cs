//
//  Log.cs
//
//  Author:
//       Bradley Bertrim <bertrim.bradley@gmail.com>
//
//  Copyright (c) 2014 Bradley Bertrim
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using UnityEngine;

namespace Bradley.Game.Core.Logging {
    [Serializable]
    public class Log {
        private String className = "";

        /// <summary>
        /// Create a Log object with the fully quallified class name set as the 
        /// context. If not namespace is defined it is omitted.
        /// </summary>
        /// <param name="Class">The class you want assossiated with this log 
        /// object.</param>
        public Log(Type Class) {
            this.className = 
                 (Class.Namespace == null ? "" : Class.Namespace + ".") 
                + Class.Name;
        }

        public void trace(String message) {
            LogManager.Instance.LogMessage(className, message, LogLevel.TRACE);
        }

        public void  debug(String message) {
            LogManager.Instance.LogMessage(className, message, LogLevel.DEBUG);
        }

        public void  info(String message) { 
            LogManager.Instance.LogMessage(className, message, LogLevel.INFO);
        }
    
        public void  warning(String message) {
            LogManager.Instance.LogMessage(className, message, LogLevel.WARNING);
        }
        
        public void  error(String message) {
            LogManager.Instance.LogMessage(className, message, LogLevel.ERROR);
        }

        public void  fatal(String message) {
            LogManager.Instance.LogMessage(className, message, LogLevel.FATAL);
        }

        private String createLogMessage(String message) {
            return className + ": " + message;
        }
    }
}

