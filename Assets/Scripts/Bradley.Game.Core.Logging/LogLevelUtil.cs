﻿//
//  LogLevelUtil.cs
//
//  Author:
//       Bradley Bertrim <bertrim.bradley@gmail.com>
//
//  Copyright (c) 2014 Bradley Bertrim
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bradley.Game.Core.Logging {
    class LogLevelUtil {

        /// <summary>
        /// Utility method for searching a list of MappedLogLevels for one with 
        /// a specific class namme
        /// </summary>
        /// <param name="className">Fully quallified class name</param>
        /// <param name="mappedLogLevels"></param>
        /// <returns>If the MappedLogLevel is found it is returned otherwise 
        /// null is returned.</returns>
        public static MappedLogLevel getMappedLogLevelByClass(
                                        string className, 
                                        List<MappedLogLevel> mappedLogLevels) {
            MappedLogLevel foundMappedLogLevel = null;
            foreach(MappedLogLevel mappedLogLevel in mappedLogLevels) {

                if(className.Equals(mappedLogLevel.ClassName)) {
                    foundMappedLogLevel = mappedLogLevel;
                    break;
                }
            }

            return foundMappedLogLevel;
        }  
    }
}
