﻿//
//  ClickController.cs
//
//  Author:
//       Bradley Bertrim <bertrim.bradley@gmail.com>
//
//  Copyright (c) 2014 Bradley Bertrim
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Bradley.Game.Core.Logging;
using Bradley.Unity.Type;
using Bradley.Game.RTS;

namespace Bradley.Game.Interface {
    public class ClickController: MonoBehaviour {
        private Log logger;
        public MouseButton selectButton = MouseButton.Left;
        public MouseButton actionButton = MouseButton.Right;
        public MouseButton manipulateButton = MouseButton.Middle;
        private List<KeyCode> clickModifiers = new List<KeyCode>();

        void Start() {
            logger = new Log(this.GetType());

            //Initalize all of the possible modifier keys to be checked
            clickModifiers.Add(KeyCode.LeftShift);
            clickModifiers.Add(KeyCode.RightShift);
            clickModifiers.Add(KeyCode.LeftAlt);
            clickModifiers.Add(KeyCode.RightAlt);
            clickModifiers.Add(KeyCode.LeftControl);
            clickModifiers.Add(KeyCode.RightControl);
        }
        
        void Update() {
            foreach(MouseButton button in Enum.GetValues(typeof(MouseButton))) {
                if(Input.GetMouseButtonDown((int)button)) { 
                    RaycastHit hit;
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    
                    if(Physics.Raycast(ray, out hit, Mathf.Infinity)) {
                        logger.debug("HIT! -> " + hit.collider.gameObject.name);
                        List<KeyCode> modifiersPressed = new List<KeyCode>();

                        foreach(KeyCode modifier in clickModifiers) {
                            if(Input.GetKey(modifier)) {
                                modifiersPressed.Add(modifier);
                            }
                        }

                        UnitManager.Instance.ClickNotification(
                                                    button, 
                                                    hit.transform.gameObject, 
                                                    hit.point, 
                                                    modifiersPressed);
                    }
                }
            }
        }
    }
}